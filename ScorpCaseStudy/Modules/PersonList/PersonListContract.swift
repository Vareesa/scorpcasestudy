//
//  PersonListContract.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

import Foundation

protocol IPersonListView: IBaseView {
    func reloadTableView()
    func showErrorAlert(message: String)
}

protocol IPersonListPresenter: IBasePresenter {
    func refreshPersonList()
    func getPersonList() -> [Person]?
    func loadMorePersonForList()
}

protocol IPersonListInteractor: AnyObject {
    func getPersonList(next: String?)
}

protocol IPersonListInteractorToPresenter: AnyObject {
    func loadMorePersonForListRetrieved(list: [Person]?, next: String?)
    func getPersonListRetrieved(list: [Person]?, next: String?)
    func apiError(error: FetchError)
}

protocol IPersonListRouter: AnyObject {
    // TODO: Declare wireframe methods
}
