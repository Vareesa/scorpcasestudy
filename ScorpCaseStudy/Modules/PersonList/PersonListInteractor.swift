//
//  PersonListInteractor.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.

import Foundation

class PersonListInteractor {

    private enum Constants {
        static let apiRetryLimit = 3
    }

    // MARK: Properties
    weak var output: IPersonListInteractorToPresenter?
    private var apiErrorCounter = 0
}

extension PersonListInteractor: IPersonListInteractor {
    func getPersonList(next: String?) {
        DataSource.fetch(next: next) { [weak self] response, error in
            guard let self = self else { return }
            let currentPage = Int(next ?? "") ?? 0

            if let error = error {
                if self.apiErrorCounter < 3 {
                    self.apiErrorCounter += 1
                    self.getPersonList(next: next)
                } else {
                    self.output?.apiError(error: error)
                }
            } else {
                self.apiErrorCounter = 0
                if currentPage == 0 {
                    self.output?.getPersonListRetrieved(list: response?.people, next: response?.next)
                } else {
                    self.output?.loadMorePersonForListRetrieved(list: response?.people, next: response?.next)
                }
            }
        }
    }
}
