//
//  PersonListViewController.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

import Foundation
import UIKit

class PersonListViewController: BaseViewController, StoryboardLoadable {

    // MARK: Properties
    @IBOutlet private weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()

    var presenter: IPersonListPresenter?
    var adapter: PersonListAdapter?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Person List"
    }
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad?()
        prepareTableView()
    }

    private func prepareTableView() {
        tableView.dataSource = adapter
        tableView.delegate = adapter
        tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0)
        tableView.registerCell(PersonTableViewCell.self)


        refreshControl.addTarget(self, action: #selector(refreshPersonList(_:)), for: .valueChanged)

        tableView.addSubview(refreshControl)
    }

    @objc private func refreshPersonList(_ sender: Any) {
        presenter?.refreshPersonList()
    }
}

extension PersonListViewController: IPersonListView {
    func showErrorAlert(message: String) {
        let title = "Error"
        let buttonText = "OK"

        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonText, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

        refreshControl.endRefreshing()
    }

    func reloadTableView() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
}
