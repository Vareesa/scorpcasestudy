//
//  PersonListRouter.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

import Foundation
import UIKit

class PersonListRouter {

    // MARK: Properties

    weak var view: UIViewController?

    // MARK: Static methods

    static func setupModule() -> PersonListViewController {
        let viewController = UIStoryboard.loadViewController() as PersonListViewController
        let presenter = PersonListPresenter()
        let router = PersonListRouter()
        let interactor = PersonListInteractor()
        let adapter = PersonListAdapter(presenter: presenter)

        viewController.presenter =  presenter
        viewController.modalPresentationStyle = .fullScreen
        viewController.adapter = adapter

        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor

        router.view = viewController

        interactor.output = presenter

        return viewController
    }
}

extension PersonListRouter: IPersonListRouter {
    // TODO: Implement wireframe methods
}
