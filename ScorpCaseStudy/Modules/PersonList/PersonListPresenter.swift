//
//  PersonListPresenter.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

import Foundation

class PersonListPresenter {

    // MARK: Properties

    weak var view: IPersonListView?
    var router: IPersonListRouter?
    var interactor: IPersonListInteractor?
    private(set) var personList: [Person]?
    private var nextPage: String?
}

extension PersonListPresenter: IPersonListPresenter {
    func getPersonList() -> [Person]? {
        personList?.unique(by: { $0.id })
    }

    func refreshPersonList() {
        interactor?.getPersonList(next: nil)
        view?.showProgressHUD()
    }

    func loadMorePersonForList() {
        interactor?.getPersonList(next: nextPage)
        view?.showProgressHUD()
    }

    func viewDidLoad() {
        refreshPersonList()
    }
}

extension PersonListPresenter: IPersonListInteractorToPresenter {
    func apiError(error: FetchError) {
        view?.showErrorAlert(message: error.errorDescription)
        view?.hideProgressHUD()
    }

    func loadMorePersonForListRetrieved(list: [Person]?, next: String?) {
        nextPage = next

        guard let list = list else { return }
        personList?.append(contentsOf: list)
        view?.reloadTableView()
        view?.hideProgressHUD()
    }

    func getPersonListRetrieved(list: [Person]?, next: String?) {
        nextPage = next

        guard let list = list else { return }
        personList = list
        view?.reloadTableView()
        view?.hideProgressHUD()
    }
}
