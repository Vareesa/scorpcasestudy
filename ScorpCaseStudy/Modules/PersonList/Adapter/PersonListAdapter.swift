//
//  PersonListAdapter.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

import Foundation
import UIKit

class PersonListAdapter: NSObject {
    private let presenter: IPersonListPresenter
    private var isEmptyState = false

    init(presenter: IPersonListPresenter) {
        self.presenter = presenter
    }
}

extension PersonListAdapter: IBaseAdapter {
    func itemCount() -> Int {
        presenter.getPersonList()?.count ?? 0
    }
}

extension PersonListAdapter: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if presenter.getPersonList() != nil && itemCount() == 0 {
             tableView.setEmptyMessage("No one here 🤌")
            isEmptyState = true
         } else {
             tableView.restore()
             isEmptyState = false
         }

        return itemCount()
    }

    // Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row

        let cell =
            tableView.dequeueReusableCell(withIdentifier: PersonTableViewCell.nameOfClass,
                                          for: indexPath)
        if let cell = cell as? PersonTableViewCell {
            cell.model = presenter.getPersonList()?[index]
        }
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let index = indexPath.row
        if !isEmptyState, index == itemCount() - 2 {
            presenter.loadMorePersonForList()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}
