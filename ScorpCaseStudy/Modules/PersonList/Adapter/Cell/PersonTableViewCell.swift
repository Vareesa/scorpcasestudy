//
//  PersonTableViewCell.swift
//  ScorpCaseStudy
//
//  Created by İsmail KILIÇ on 30.09.2021.
//  Copyright © 2021 İsmail KILIÇ. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet private weak var lblLocation: UILabel!

    var model: Person? {
        didSet {
            lblLocation.text = "\(model?.fullName ?? "")(\(model?.id ?? 0))"
        }
    }
}
